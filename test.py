import spacy
nlp = spacy.load("en_core_web_sm")


def is_suitable_for_tweet(name):
    doc = nlp(name)
    for ent in doc.ents:
        if ent.label_ == 'PERSON': return False  # return false if person
    return True  # return true

name = 'PantherSwap #BSC'
bio = r"The First Automatic Liquidity Acquisition Yield Farm & AMM on #BSC With Unique Rewards Lockup & Anti Whale Mechanism"

# printing the result
print(is_suitable_for_tweet(name=name + ' ' + bio))