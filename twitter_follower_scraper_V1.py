import tweepy  # python wrapper for twitter
import gspread  # module for reading and writing google sheet.
from oauth2client.service_account import ServiceAccountCredentials  # for authorizing google spreadsheet api
from datetime import datetime, timedelta
import spacy  # for detecting name


class TwitterFriendsScraper:

    # 1. Initialization of all required variable
    def __init__(self):
        self.consumer_key = "Qyh8bVXh05SB5h5uXLe8AsvPc"
        self.consumer_secret = "F69Kms4FPSLSHYmgy9R2frDIzo5rYJvYa7xRITcKB7AXpTVogK"
        self.access_token = "1300906999274639360-7Mel3iKtKQ4JkeHqtfefQG3dbzG3eF"
        self.access_token_secret = "442pZHYsWg6CTWqH8CdDTSJFAi5uTXwIGXlhbhPkspNPe"
        self.nlp = spacy.load("en_core_web_sm")  # run the command to download language model -> spacy download en
        self.google_spreadsheet = self.connect_to_google_sheet()  # google spreadsheet object
        self.usernames = self.read_usernames()  # usernames from Usernames sheet
        self.already_existing_friends = self.read_existing_screen_names()
        self.api_client = self.authorize_twitter_api_credentials()  # authorized twitter api client for making api call
        self.friend_list = []  # empty list for storing scraped data

    # 2. method for authorizing google sheet using api credentials
    def connect_to_google_sheet(self):
        print('Authorizing google sheet...')
        spreadsheet_credentials = {
            "type": "service_account",
            "project_id": "seeking-alpha-313005",
            "private_key_id": "5c92c4e5fcb4ab85fa528a3bfcfc13953e9ef382",
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCiR0FKV0pkATkX\nkDAM/pe4hfeYuVeN4qXWQHmuUxOshEVADqv7bVQDVUeCp3OO7mobg7wHHrRSiR3B\nyB2fkBWjgGmI9aKL0cJPWZqY3CxmhAUtb9KWTdMBM/LyMYqOSd5RSGdcsuZWKLKo\ntrw4c23hPTmtyPYz34Ajh6DUqdfGg0Mzy1gMTK0nwkjSB+/yn//Jia69pkQjubUm\na1HvtlM3bPwvlnEzk1cjELb4cTdftuIkEd9h1Nhm0W7+vD+JDMa+ztEOZo2hqSiz\ngtG95hN3KIv/RneLQDYaavQNphwNnkw4YfHglP21ItMk2V6jWBYq1bAggueH39Q5\nsKhOPUwFAgMBAAECggEABLIDm1tNuXmIlZw0K7gVGz9zE83vENmpOzsH2kmolNIJ\n7+nMw5xc9TC7PjZKo2zWEKA96UXT6VxyyjfYVZTVb3LoetBCqQGDegjJbplWRowU\n20T0RqmPTKZcZfIVhHryLg9mgDnj7MPGMZgB968tU3pSSCs7pnIV0tFaj7SvW9zG\nIj/nyMmOxpdBdVYedvxru0m2sXkIaq+587htp6b/3hW00EnSt0MGwGDXDW7tqe8N\n4KyVY7dtESYNYlHhJO+cLa7haucItpDxV7P+nyYu59u9T0UwHvO/naAI90qaRjrE\ntws8NQWw1ik8456vbnrqvp7rm82Itf/iBNzE1MssoQKBgQDMH7AGxZUBoUnK5FOw\nL8poBSoVNyB4dgQx+HBIJCtS+36AIGhHfU7UhcpY3sg8UhOcI7cGkIKVeUyW2JJs\nPcOGkpJ2BlrvvoTMXx/v0muXlOgs2Juzocw4O+qCHTlwG/a4c/Uh8LdJ2wDiP+Y4\ncJVNaZGXnL/9wRivdqbv1kNz5QKBgQDLhRljnFS0Pyajt46tb+PoVftcv7MjTMHh\nWgIB/N2nbJ/B9T3BV8zT93p8+qVJ6e8JJs1jfjfL2EYHlhRMFFbWOR7gpX/favfS\nZ3GWd6Nh7H36Xqj1GgnEx6HP9WkNK0LwktoQQ9oTluQRpt86skq8ZaTctju9QS3y\nNCpmJFc1oQKBgFr0G6lIzPZm+cyQJMIy6UukUXYWlD9YxtHFU7KxeySEr39D8XM5\nYLDN1LX4vtwx7k76kwk3xxf3bqsgYU4dz0vdb6lrd4WwLNgr4r0n0409ap7g/a/y\n/l5oRDwp7XgI3cyezrGK4wDjtucZdencMIrH6XnW/K2DJw64HfZsUK4JAoGAO4i0\n05mwgYWTpQKV/xAjCBzWydvd6C6U7Hnq6m/w2wAAf7lglkU7yqmzMAoV/dgrr26l\nWDxc0Al0+tUhHNw6RrIw4SAols9wrSDLbmuS5JtMgsfb/s7eGq0XHYE5eLu1+i/L\nEET47xANqUg6Nq5fBFtrbZD1SrhdW2N9H6HzzOECgYAyF12YW5qDdXx8EzvJkO2A\nGb92qw2BOJ65J58a9Vn3/N/sb0QBn1GJx5TeNVOLQZ/rUMF4fvi6sEN5SrHEOaZk\nz671bJPYr1wIYRpP1LURAIMx7ahqoOPaIxPrekCsotYb8NAa6KWmZa67kIs6PLzH\nXGrPEe+QBxyS6w8QXKkt2Q==\n-----END PRIVATE KEY-----\n",
            "client_email": "python@seeking-alpha-313005.iam.gserviceaccount.com",
            "client_id": "108496873584712066359",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/python%40seeking-alpha-313005.iam.gserviceaccount.com"
        }
        scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        creds = ServiceAccountCredentials.from_json_keyfile_dict(spreadsheet_credentials, scope)
        client = gspread.authorize(creds)
        # configure google spreadsheet name bellow. make sure you  have shared the spreadsheet with client_email
        spreadsheet = client.open('result alpha')
        print('Google sheet authorization successful.\n')
        return spreadsheet

    # 3. method for reading username from sheet Usernames
    def read_usernames(self):
        print('Reading usernames...')
        # getting Input sheet
        st = self.google_spreadsheet.worksheet('Input')
        # reading first column values in a list
        usernames = st.col_values(1)
        print(f'{len(usernames)} username found.\n')
        return usernames

    # 4. method for reading already scraped data from google sheet.
    def read_existing_screen_names(self):
        print('Reading existing screen names...')
        # getting Output sheet
        st = self.google_spreadsheet.worksheet('Output')
        # reading second column values in a list
        screen_names = st.col_values(2)
        print(f'{len(screen_names)} row found.\n')
        return screen_names

    # 5. method for authorizing twitter api client
    def authorize_twitter_api_credentials(self):
        print('Authorizing twitter API client...')
        # authorization of consumer key and consumer secret
        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        # set access to user's access key and access secret
        auth.set_access_token(self.access_token, self.access_token_secret)
        # calling the api
        api = tweepy.API(auth, wait_on_rate_limit=True)
        print('Twitter API client authorization successful.\n')
        return api

    # 6. method for getting friends for each username
    def get_friends(self, user_screen_name):
        for friend in tweepy.Cursor(self.api_client.friends, user_screen_name).items(5):
            if friend.screen_name in self.already_existing_friends: continue
            try:
                scraping_time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
                diff = datetime.now() - friend.created_at
                time_duration = self.get_duration(time_diff=diff)
                # generating twitter text columns
                if friend.description == "":
                    twitter_text = f'.@{friend.screen_name} ({friend.followers_count} followers)\nJoined {time_duration}'
                else:
                    twitter_text = f'.@{friend.screen_name} ({friend.followers_count} followers)\n\n{friend.description}\n\nJoined {time_duration}'
                friend_obj = [friend.name, friend.screen_name, friend.description, friend.created_at.strftime("%d/%m/%Y %H:%M:%S"), friend.followers_count, friend.friends_count, f'https://twitter.com/{friend.screen_name}', friend.url, user_screen_name, scraping_time, twitter_text]
                self.friend_list.append(friend_obj)
                if self.is_suitable_for_tweet(duration=diff, name=friend.name + ' ' + friend.description):
                    self.api_client.update_status(twitter_text)
            except Exception:
                pass

    # 7. main method for looping through all the username and calling get_friends method
    def main(self):
        for username in self.usernames:
            print('Scraping friends of :', username)
            self.get_friends(user_screen_name=username)

    # 8. method for exporting scraped data to google sheet.
    def export_to_google_spreadsheet(self):
        if self.friend_list:
            print('\nWriting scraped data to Friends sheet...')
            self.google_spreadsheet.values_append(
                'Output',
                params={'valueInputOption': 'USER_ENTERED'},
                body={'values': self.friend_list}
            )
            print('Data exported successfully')
        else:
            print('\nNo new friend found.')

    # 9. method for getting duration
    def get_duration(self, time_diff):
        days = time_diff.days
        if days > 29:
            m = days // 30
            d = days % 30
            strm = ' months ' if m > 1 else ' month '
            strd = ' days ' if d > 1 else ' day '
            if d == 0:
                return str(m) + strm + 'ago'

            return str(m) + strm + str(d) + strd + 'ago'
        else:
            strd = ' days ' if days > 1 else ' day '
            return str(days) + strd + 'ago'

    # 10. method for checking eligibility for tweet
    def is_suitable_for_tweet(self, duration, name):
        if duration.days >= 180: return False  # return false if duration more then six months
        doc = self.nlp(name)
        for ent in doc.ents:
            if ent.label_ == 'PERSON': return False  # return false if person
        return True  # return true


if __name__ == '__main__':
    scraper = TwitterFriendsScraper()
    scraper.main()
    scraper.export_to_google_spreadsheet()
