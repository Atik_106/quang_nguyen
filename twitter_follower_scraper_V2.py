import tweepy
from airtable import Airtable
from datetime import datetime, timedelta
import spacy  # for detecting name
import sys

class TwitterFriendsScraper:

    # 1. Initialization of all required variable
    def __init__(self):
        self.consumer_key = "Qyh8bVXh05SB5h5uXLe8AsvPc"
        self.consumer_secret = "F69Kms4FPSLSHYmgy9R2frDIzo5rYJvYa7xRITcKB7AXpTVogK"
        self.access_token = "1300906999274639360-7Mel3iKtKQ4JkeHqtfefQG3dbzG3eF"
        self.access_token_secret = "442pZHYsWg6CTWqH8CdDTSJFAi5uTXwIGXlhbhPkspNPe"
        self.nlp = spacy.load("en_core_web_sm")  # run the command to download language model -> spacy download en
        self.output_table = Airtable(base_key='appqgkf5wwjl0KCRp', api_key='keyRWkaLlh6x4ofOp', table_name='Output')  # getting Output table
        self.usernames = self.read_usernames()  # usernames from Input sheet
        self.already_existing_friends = self.read_existing_screen_names()
        self.api_client = self.authorize_twitter_api_credentials()  # authorized twitter api client for making api call
        self.friend_list = []  # empty list for storing scraped data

    # 2. method for reading username from input table
    def read_usernames(self):
        print('Reading usernames...')
        # getting Input table
        input_table = Airtable(base_key='appqgkf5wwjl0KCRp', api_key='keyRWkaLlh6x4ofOp', table_name='Input')
        # reading first column values in a list
        records = input_table.get_all(fields=['Username'])
        usernames = [record['fields']['Username'] for record in records]
        print(f'{len(usernames)} username found.\n')
        return usernames

    # 3. method for reading already scraped username from airtable.
    def read_existing_screen_names(self):
        print('Reading existing screen names...')
        # reading second column values in a list
        records = self.output_table.get_all(fields=['Username'])
        screen_names = [record['fields']['Username'] for record in records]
        print(f'{len(screen_names)} row found.\n')
        return screen_names

    # 4. method for authorizing twitter api client
    def authorize_twitter_api_credentials(self):
        print('Authorizing twitter API client...')
        # authorization of consumer key and consumer secret
        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        # set access to user's access key and access secret
        auth.set_access_token(self.access_token, self.access_token_secret)
        # calling the api
        api = tweepy.API(auth, wait_on_rate_limit=True)
        print('Twitter API client authorization successful.\n')
        return api

    # 5. method for getting friends for each username
    def get_friends(self, user_screen_name):
        for friend in tweepy.Cursor(self.api_client.friends, user_screen_name).items(5):
            if friend.screen_name in self.already_existing_friends: continue
            try:
                scraping_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.000Z")
                # getting time duration
                diff = datetime.now() - friend.created_at
                time_duration = self.get_duration(time_diff=diff)
                # generating twitter text columns
                if friend.description == "":
                    twitter_text = f'.@{friend.screen_name} ({friend.followers_count} followers)\nJoined {time_duration}'
                else:
                    twitter_text = f'.@{friend.screen_name} ({friend.followers_count} followers)\n\n{friend.description}\n\nJoined {time_duration}'
                # building airtable record object
                friend_record = {
                    'Name': friend.name,
                    'Username': friend.screen_name,
                    'Description': friend.description,
                    'Join Date': friend.created_at.strftime("%Y-%m-%dT%H:%M:%S.000Z"),
                    '#Followers': friend.followers_count,
                    '#Following': friend.friends_count,
                    'Profile URL': f'https://twitter.com/{friend.screen_name}',
                    'URL': friend.url,
                    'inquiry': user_screen_name,
                    'Timestamp': scraping_time,
                    'Twitter Text': twitter_text,
                }
                self.friend_list.append(friend_record)
                if self.is_suitable_for_tweet(duration=diff, name=friend.name + ' ' + friend.description):
                    self.api_client.update_status(twitter_text)
            except Exception as e:
                with open('error.txt', 'a', encoding='utf-8') as err_file:
                    err_file.write(str(e))

    # 6. main method for looping through all the username and calling get_friends method
    def main(self):
        for username in self.usernames:
            print('Scraping friends of :', username)
            try:
                self.get_friends(user_screen_name=username)
            except Exception as e:
                with open('error.txt', 'a', encoding='utf-8') as err_file:
                    err_file.write(str(e))

    # 7. method for exporting scraped data output table.
    def export_to_airtable(self):
        if self.friend_list:
            while self.friend_list:
                if len(self.friend_list) > 5:
                    chunk = self.friend_list[:5]
                else:
                    chunk = self.friend_list
                self.friend_list = self.friend_list[len(chunk):]
                self.output_table.batch_insert(chunk)
            print('Data exported successfully')
        else:
            print('\nNo new friend found.')

    # 8. method for getting duration
    def get_duration(self, time_diff):
        days = time_diff.days
        if days > 29:
            m = days // 30
            d = days % 30
            strm = ' months ' if m > 1 else ' month '
            strd = ' days ' if d > 1 else ' day '
            if d == 0:
                return str(m) + strm + 'ago'

            return str(m) + strm + str(d) + strd + 'ago'
        else:
            strd = ' days ' if days > 1 else ' day '
            return str(days) + strd + 'ago'

    # 9. method for checking eligibility for tweet
    def is_suitable_for_tweet(self, duration, name):
        if duration.days >= 180: return False  # return false if duration more then six months
        doc = self.nlp(name)
        for ent in doc.ents:
            if ent.label_ == 'PERSON': return False  # return false if person
        return True  # return true


if __name__ == '__main__':
    scraper = TwitterFriendsScraper()
    scraper.main()
    scraper.export_to_airtable()
